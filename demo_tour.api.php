<?php

/**
 * @file
 * API documentation for Demo Tour.
 */

/**
 * Allows other modules to alter tours.
 *
 * @param array $tours
 *
 */
function hook_demo_tour_tours_alter(&$tours) {
}

/**
 * Allows other modules to alter an individual tour as it is being added to the page.
 *
 * @param array $tour
 *
 */
function hook_demo_tour_tour_alter(&$tour) {
}
