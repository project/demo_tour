<?php

/**
 * @file
 * Admin functionality for Demo Tour.
 */

function demo_tour_admin_form() {
  $form['demo_tour_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay'),
    '#description' => t('Time (in milliseconds) before the demo tours should be triggered.'),
    '#default_value' => variable_get('demo_tour_delay', 1000),
  );

  return system_settings_form($form);
}
