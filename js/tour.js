/*jslint nomen: true, plusplus: true, todo: true, white: true, browser: true, indent: 2 */

(function ($) {
  'use strict';
  Drupal.behaviors.demoTour = {
    attach: function (context, settings) {

      setTimeout(function() {
        if (Drupal.settings.demoTour && window === window.parent) {

          $.cookie.json = true;

          var firstTour;
          var demoTours = [];
          var tours = Drupal.settings.demoTour.tours;

          var toursTaken = JSON.parse($.cookie('demoToursTaken')) || [];

          var availableTours = [];

          // Generate a bootstrap-tour for each tour
          for (var tid in tours) {
            var thisTour = tours[tid];

            // Check the cookies to see if the user has already taken this tour.
            var alreadyTaken = $.inArray(tid, toursTaken) > -1;

            // Check if the tour should play for this screen size.
            var mediaQuery = thisTour.mediaQuery;
            var mediaQueryMatches = true;
            if (!!mediaQuery) {
              mediaQueryMatches = matchMedia(mediaQuery).matches;
            }

            // Check if the tour's elements exist and are visible on the page.
            var elementsExist = true;
            for (var stepId in thisTour.steps) {
              var $element = $(element);
              if ($element.length == 0 || !$element.is(":visible")) {
                elementsExist = false;
                break;
              }
            }

            if (!alreadyTaken && mediaQueryMatches && elementsExist) {

              availableTours[tid] = thisTour;

              availableTours[tid].template = function (i, step) {
                var that = tours[step.tid];
                var html;
                html = '<div class="popover tour-tour fade right in demo_tour_' + step.tid + '_step-' + i + '">';
                html += '  <div class="popover-arrow"></div>';
                html += '  <div class="popover-close" data-role="end"><span>[X]</span></div>';
                html += '  <h4><span class="popover-title">' + step.title + '</span></h4>';
                html += '  <div class="popover-content"><p></p></div>';
                html += '  <div class="popover-navigation">';

                var label, role;
                if (i == (that.steps.length - 1)) {
                  label = Drupal.t('Get started');
                  role = 'end';
                }
                else {
                  label = Drupal.t('Next');
                  role = 'next';
                }

                html += '    <button data-role="' + role + '" class="tour-next">' + label + '</button>';
                html += '    <div class="popover-footer"></div>';
                html += '  </div>';
                html += '</div>';
                return html;
              };

              availableTours[tid].run = function () {
                var that = this;

                this.tour = new Tour({
                  // TODO: Make some of these options configurable
                  backdrop: that.modal,
                  redirect: false,
                  orphan: true,
                  container: '#main-body', // Protect iframes
                  duration: false,
                  template: this.template,
                  onEnd: function (tour) {
                    var next = demoTours.shift();
                    if (next) {
                      next.run();
                    }
                  },
                  onShown: function (tour) {
                    // If we set margin-left directly on CSS, it won't work on Firefox
                    // FIXME: Argh, hard-coded
                    jQuery('.tour-step-background').css('margin-left', '-200px');

                    // Do not hide popovers on mouse over
                    jQuery('#' + this.id).off('mouseenter').off('mouseleave');

                    toursTaken.push(that.tid);

                    // Store a cookie so as not to show this tour again.
                    $.cookie('demoToursTaken', JSON.stringify(toursTaken), { expires: 7, path: '/' });
                  }
                });

                this.tour.addSteps(this.steps);
                this.tour.init();

                try {
                  this.tour.start(true);
                } catch (e) {
                  // Avoid errors with elements that were not completely loaded
                }
              };

              if (!firstTour) {
                firstTour = availableTours[tid];
              }
              else {
                demoTours.push(availableTours[tid]);
              }
            }
          }
        }

        // Run the tours, if any are relevant.
        if (!!firstTour) {
          firstTour.run();
        }
      }, Drupal.settings.demoTour.delay);

    }
  }


})(jQuery);
