<?php
/**
 * @file
 * Install file for demo_tour
 */

/**
 * Implements hook_schema().
 */
function demo_tour_schema() {

  // Tours are stored here
  $schema['demo_tour_presets'] = array(
    'description' => 'Presets for the demo_tour module',
    'export' => array(
      'key' => 'name',
      // Exports will be as $preset.
      'identifier' => 'preset',
      // Function hook name.
      'default hook' => 'default_demo_tour_presets',
      'api' => array(
        'owner' => 'demo_tour',
        // Base name for api include files.
        'api' => 'default_demo_tour_presets',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'id' => array(
        'description' => 'Internal primary key',
        'type' => 'serial',
        'not null' => TRUE,
        'no export' => TRUE,
      ),
      'name' => array(
        'description' => 'Machine name',
        'type' => 'varchar',
        'length' => '128',
        'not null' => TRUE,
      ),
      'title' => array(
        'description' => 'Readable name',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
      ),
      'language' => array(
        'description' => 'Language of this tour',
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
      'media_query' => array(
        'description' => 'Media query of this tour',
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
      'roles' => array(
        'description' => 'Roles that have access to this tour',
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
      'paths' => array(
        'description' => 'Paths of this tour',
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => FALSE,
      ),
      'steps' => array(
        'description' => 'Steps of this tour',
        'type' => 'text',
        'not null' => FALSE,
        'serialize' => TRUE,
      ),
      'auto' => array(
        'description' => 'Whether this tour should be taken automatically by users',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
      'modal' => array(
        'description' => 'Whether this tour should disable interaction with other elements',
        'type' => 'int',
        'not null' => FALSE,
        'default' => 0,
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );

  return $schema;
}

/**
 * Adds a "paths" field to the presets table.
 */
function demo_tour_update_7100() {
  $ret = array();
  $spec = array(
    'description' => 'Paths of this tour',
    'type' => 'text',
    'not null' => FALSE,
    'serialize' => FALSE,
  );
  db_add_field('demo_tour_presets', 'paths', $spec, $ret);
  return $ret;
}

/**
 * Changes type of "roles" column
 */
function demo_tour_update_7101() {
  $spec = array(
    'description' => 'Roles that have access to this tour',
    'type' => 'text',
    'not null' => FALSE,
    'serialize' => TRUE,
  );
  return db_change_field('demo_tour_presets', 'roles', 'roles', $spec);
}

/**
 * Adds a language field to the presets table.
 */
function demo_tour_update_7102() {
  $spec = array(
    'description' => 'Language of this tour',
    'type' => 'text',
    'not null' => FALSE,
    'serialize' => TRUE,
  );
  db_add_field('demo_tour_presets', 'language', $spec);
}

/**
 * Adds a modal field to the presets table.
 */
function demo_tour_update_7103() {
  $spec = array(
    'description' => 'Whether this tour should disable interaction with other elements',
    'type' => 'int',
    'not null' => FALSE,
    'default' => 0,
  );
  db_add_field('demo_tour_presets', 'modal', $spec);
}

/**
 * Adds a media query field to the presets table.
 */
function demo_tour_update_7104() {
  $spec = array(
    'description' => 'Media query of this tour',
    'type' => 'text',
    'not null' => FALSE,
  );
  db_add_field('demo_tour_presets', 'media_query', $spec);
}
